# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2019-09-10

### Added
- This changelog
- Configured Gitlab CI

### Fixed
- Correct code size for Teensy 4.0 (https://gitlab.com/teensy-rs/teensy-loader-rs/merge_requests/1)

## [0.1.1] - 2019-09-09

### Fixed
- Correct real name in `Cargo.toml`

## [0.1.0] - 2019-09-09

Initial import
