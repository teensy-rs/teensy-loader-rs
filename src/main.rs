extern crate clap;
extern crate elf;
extern crate env_logger;
extern crate failure;
extern crate log;
extern crate rusb;

mod payload;
mod teensy;

use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

use clap::{crate_version, App, Arg};
use failure::{format_err, Fallible, ResultExt};
use log::{debug, info, trace};

use payload::Payload;

fn main() -> Fallible<()> {
    env_logger::init();

    let matches = App::new("Rust Teensy Loader")
        .about("Rust reimplementation of teensy_loader_cli")
        .version(crate_version!())
        .arg(
            Arg::with_name("binary")
                .short("b")
                .long("to-binary")
                .value_name("PATH")
                .help("Dumps content that would be uploaded to Teensy as a raw binary file")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("wait")
                .short("w")
                .long("wait")
                .help("Waits for a teensy to become available instead of exiting"),
        )
        .arg(
            Arg::with_name("list")
                .short("l")
                .long("list")
                .help("Lists supported Teensy models"),
        )
        .arg(
            Arg::with_name("teensy")
                .short("t")
                .long("teensy")
                .help("Set teensy model (use --list to list supported models)")
                .value_name("MODEL")
                .takes_value(true)
                .required_unless_one(&["list", "binary"]),
        )
        .arg(
            Arg::with_name("INPUT")
                .help("Input file (in ELF format)")
                .required_unless_one(&["list"]),
        )
        .get_matches();

    if matches.is_present("list") {
        println!("Supported Teensy models:");
        for model in teensy::Model::iter() {
            println!(" -t {}\t- {}", model.version, model);
        }
        return Ok(());
    }

    debug!("START");

    let path = PathBuf::from(matches.value_of("INPUT").unwrap());

    trace!("Reading payload {:?}", path);
    let payload = Payload::read_elf_file(path).context("Reading input file")?;
    debug!("Input file loaded");

    if matches.is_present("binary") {
        let outpath = PathBuf::from(matches.value_of("binary").unwrap());
        let mut outf = File::create(&outpath).context("Creating output file")?;
        debug!("Writing {:?}", outpath);
        // TODO: optionally resize/seek to payload.base_addr?
        outf.write_all(&payload.to_bin())
            .context("Writing output file")?;
        return Ok(());
    }

    let model_name = matches.value_of("teensy").unwrap();
    let model = teensy::Model::get(model_name).ok_or_else(|| {
        format_err!(
            "Unknown model {:?}; use '--list' to list supported models",
            model_name
        )
    })?;

    debug!("Selected model: {}", model);

    let mut dev =
        teensy::Teensy::open(model, matches.is_present("wait")).context("Opening Teensy")?;

    info!("Found HalfKay");

    dev.write_payload(&payload).context("Writing payload")?;
    dev.boot().context("Rebooting into user code")?;

    Ok(())
}
